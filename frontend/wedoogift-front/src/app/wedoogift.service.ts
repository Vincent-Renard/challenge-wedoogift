import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { WedoogiftApiConfig } from './api-config.model';
import { Observable } from 'rxjs';
import { CardResponse } from './models/card-response.model';

@Injectable({
  providedIn: 'root',
})
export class WedoogiftService {

  private token: string = 'tokenTest123'
  constructor(
    private httpClient: HttpClient,
    private api: WedoogiftApiConfig
  ) {}
  //GET http://localhost:3000/shop/[shopId]/search-combination

  getCardsFromAmount(shopId: number, amount: number): Observable<CardResponse> {
    let params = new HttpParams().set('amount', amount);


  return this.httpClient.get<CardResponse>(
      `${this.api.url}shop/${shopId}/search-combination/`,
      {params:params,headers:{'Authorization':this.token}}
    )
    
  }
}
