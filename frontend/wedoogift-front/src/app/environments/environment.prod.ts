import { Environment } from './environement.model';

export const environment: Environment = {
  production: true,

  api: {
    url: 'http://localhost:3000/',
  },
};
