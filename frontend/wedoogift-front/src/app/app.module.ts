import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { WedoogiftApiConfig } from './api-config.model';

import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { AmountInputComponent } from './amount-input/amount-input.component';
import { CardDisplayComponent } from './card-display/card-display.component';

@NgModule({
  declarations: [AppComponent, AmountInputComponent, CardDisplayComponent],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: WedoogiftApiConfig,
      useValue: environment.api,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
