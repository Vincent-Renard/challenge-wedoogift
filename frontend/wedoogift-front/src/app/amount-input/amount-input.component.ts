import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, AbstractControl } from '@angular/forms';
import { CardResponse } from '../models/card-response.model';
import { Combination } from '../models/combination.model';
import { WedoogiftService } from '../wedoogift.service';

@Component({
  selector: 'app-amount-input',
  templateUrl: './amount-input.component.html',
  styleUrls: ['./amount-input.component.css'],
})
export class AmountInputComponent implements OnInit {
  amountForm = this.formBuilder.group({
    amount: this.formBuilder.control('', [Validators.required]),
  });
  private shopId = 5;

  cards: CardResponse | undefined;
  equal: Combination | undefined;
  ceil: Combination | undefined;
  floor: Combination | undefined;
  constructor(
    private formBuilder: FormBuilder,
    private wedoogiftService: WedoogiftService
  ) {}
  ngOnInit(): void {}

  resetCardComponents(): void {
    this.equal = undefined;
  }

  pushAmount(): void {
    this.amountForm.markAllAsTouched();
    if (this.amountForm.valid) {
      this.resetCardComponents();
     
      var amountValue: number = this.amountForm.get('amount')?.value;

      this.wedoogiftService
        .getCardsFromAmount(this.shopId, amountValue)
        .subscribe((response) => {
           this.cards=response;
          if (response.equal !== undefined) {
            this.equal = response.equal;
          }
          this.floor = response.floor;
          this.ceil=response.ceil;
        });
    }
  }
  nextMinusValue(): void {
    this.choice(this.cards!.floor.value);
  }
  nextPlusValue(): void {
    this.choice(this.cards!.ceil.value);
  }

  choice(value: number): void {
    this.amountForm.setValue({ amount: value });
    this.pushAmount();
  }
  get amount(): AbstractControl {
    return this.amountForm.get('amount')?.value;
  }
}
