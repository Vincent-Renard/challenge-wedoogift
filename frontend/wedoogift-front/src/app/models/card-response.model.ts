import { Combination } from "./combination.model"


export interface CardResponse{

    equal:Combination;
    floor:Combination;
    ceil:Combination;
}