import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Combination } from '../models/combination.model';

@Component({
  selector: 'app-card-display',
  templateUrl: './card-display.component.html',
  styleUrls: ['./card-display.component.css'],
})
export class CardDisplayComponent implements OnInit {
  @Input()
  combination!: Combination;

  @Output()
  select = new EventEmitter<number>();
  constructor() {}

  push(value: number) {
    this.select.emit(value);
  }

  ngOnInit(): void {}
}
