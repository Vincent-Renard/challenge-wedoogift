// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { Environment } from "./environement.model";

export const environment :Environment = {
  production: false,
   api: {
    url: 'http://localhost:3000/',
  }
};
