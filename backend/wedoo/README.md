# Challenge Wedoogift

## Backend

**Java Version : 17 LTS**
For run tests :
`mvn test`

### Level 1

Tests lay into _EndowmentServiceTest_

#### Features

Tests :

* _EndowmentServiceTest_

Function 1 : EndowmentService.distributeGiftCard()
Function 2 : EndowmentService.balanceOfUser()

### Level 2

**Changelog**

* Refactoring EndowmentService in User/GiftCard/MealVoucherService
* Endowment abstract entity
* GidtCard & MealVoucher 'sons

Tests :

* _UserServiceTest_
* _GiftCardServiceTest_
* _MealVoucherServiceTest_
* _MealVoucherTest_ (tests for expiration date generator)

**Food card** -> MealVoucher  
Function  : MealVoucherService.distributeMealVoucher()

### Level 3

#### URIs TABLE

| URI | VERB | CODE | REQUEST BODY | RESPONSE BODY | Authentication |   
|-----|------|------|--------------|---------------|----------------|
| /api/users/{id}/balance | **GET**   | 200 / 404 | 0 | {balance:int} | Yes {USER} |   
| /api/companies/{idC}/users/{idU}/food | **POST**   | 204 / 404 / 409 | {amount:int}| 0 | Yes {Company} |   
| /api/companies/{idC}/users/{idU}/gifts | **POST**   | 204 / 404 / 409 | {amount:int}| 0 | Yes {Company} |   
| /api/api/login?username={username}&password={password} | **POST**   | 200 | {}| 0 _Authorization_ : Bearer | No |  

#### Users Registered

| USERNAME | PASSWORD | ROLES |
|----------|----------|-------|
| Richard | Riri  |   {USER} |   
| Firmin | Fifi  |   {USER} |
| Louis | Loulou  |   {USER} |
| Wedoogift | wdg  |   {Company} |   
| Wedoofood | wdf  |   {Company} |   
