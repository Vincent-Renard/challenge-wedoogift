package org.example.wedoodemo.user.controler;

import org.example.wedoodemo.SecurizedTestUtil;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.TestConstants;
import org.example.wedoodemo.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;
import java.util.Random;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Rollback
class UserControllerTest extends SecurizedTestUtil implements TestConstants {


	private static final String URL = "/api/users/{idUser}/balance";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService serviceMock;

	private Random random = new Random();


	@Test
	void getUserBalance_givenIdUser_expect200OK() throws Exception {

		var userToken = getUserToken();
		var idUser = USER_IDS_OK[2];
		var userBalance = random.nextInt(Integer.MAX_VALUE);


		when(serviceMock.balanceOfUser(idUser)).thenReturn(userBalance);
		mockMvc.perform(get(URL, idUser)
						.header(HttpHeaders.AUTHORIZATION, userToken))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("balance")
						.value(userBalance));
	}

	@Test
	void getUserBalance_givenIdUserKO_expect404() throws Exception {
		var userToken = getUserToken();
		var idUser = USER_ID_UNKNOWN;

		when(serviceMock.balanceOfUser(USER_ID_UNKNOWN)).thenThrow(new NoSuchUserException(idUser));
		mockMvc.perform(get(URL, idUser)
				.header(HttpHeaders.AUTHORIZATION, userToken)
		).andExpect(status().isNotFound());
	}


	@Test
	void getUserBalance_givenIdUserOK_WithInvalidToken_expect401() throws Exception {
		var idUser = USER_ID_UNKNOWN;

		when(serviceMock.balanceOfUser(USER_ID_UNKNOWN)).thenThrow(new NoSuchUserException(idUser));
		mockMvc.perform(get(URL, idUser)
				.header(HttpHeaders.AUTHORIZATION, "NOPE")
		).andExpect(status().isUnauthorized());
	}

	@Test
	void getUserBalance_givenIdUserOK_WithoutToken_expect401() throws Exception {
		var idUser = USER_ID_UNKNOWN;

		when(serviceMock.balanceOfUser(USER_ID_UNKNOWN)).thenThrow(new NoSuchUserException(idUser));
		mockMvc.perform(get(URL, idUser)
		).andExpect(status().isUnauthorized());
	}

	@Test
	void getUserBalance_givenIdUserOK_WithCompanyToken_expect403() throws Exception {
		var companyToken = getCompanyToken();
		var idUser = USER_ID_UNKNOWN;

		when(serviceMock.balanceOfUser(USER_ID_UNKNOWN)).thenThrow(new NoSuchUserException(idUser));
		mockMvc.perform(get(URL, idUser)
				.header(HttpHeaders.AUTHORIZATION, companyToken)
		).andExpect(status().isForbidden());
	}
}