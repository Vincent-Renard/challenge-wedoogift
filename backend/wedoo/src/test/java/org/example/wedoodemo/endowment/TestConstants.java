package org.example.wedoodemo.endowment;

public interface TestConstants {

	int VALUE_GIVEN_TO_USER_1 = 50;
	int USER_ID_UNKNOWN = 4;
	int INITIAL_BALANCE_WEDOOFOOD = 3000;
	int INITIAL_BALANCE_WEDOOGIFT = 1000;
	int[] USER_IDS_OK = {1, 2, 3};
	int INITIAL_AMOUNT_GIVEN_TO_USER_1 = 100;
	int COMPANY_ID_UNKNOWN = 3;
	int[] COMPANIES_IDS_OK = {1, 2};

}
