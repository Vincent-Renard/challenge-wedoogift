package org.example.wedoodemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


@AutoConfigureMockMvc
public class SecurizedTestUtil implements AuthConstants {

	@Autowired
	private MockMvc mockMvc;

	private String userValidToken;
	private String companyValidToken;


	public String getUserToken() throws Exception {
		var credentialNumber = 0;
		if (userValidToken == null) {

			userValidToken = mockMvc.perform(MockMvcRequestBuilders.post(AUTH_URL)
							.queryParam("username", USER_USERNAMES_WITH_USER_ROLE[credentialNumber])
							.queryParam("password", USER_PASSWORD_WITH_USER_ROLE[credentialNumber]))
					.andReturn()
					.getResponse()
					.getHeader(HttpHeaders.AUTHORIZATION);
		}

		return userValidToken;
	}

	public String getCompanyToken() throws Exception {
		var credentialNumber = 0;
		if (companyValidToken == null) {

			companyValidToken = mockMvc.perform(MockMvcRequestBuilders.post(AUTH_URL)
							.queryParam("username", USER_USERNAMES_WITH_COMPANY_ROLE[credentialNumber])
							.queryParam("password", USER_PASSWORD_WITH_COMPANY_ROLE[credentialNumber]))
					.andReturn()
					.getResponse()
					.getHeader(HttpHeaders.AUTHORIZATION);
		}

		return companyValidToken;
	}

}
