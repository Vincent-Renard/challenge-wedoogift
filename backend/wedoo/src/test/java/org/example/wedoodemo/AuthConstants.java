package org.example.wedoodemo;

public interface AuthConstants {

	String AUTH_URL = "/api/login";
	String USER_USERNAMES_WITH_USER_ROLE[] = {"Richard", "Firmin", "Louis"};
	String USER_PASSWORD_WITH_USER_ROLE[] = {"Riri", "Fifi", "Loulou"};

	String USER_USERNAMES_WITH_COMPANY_ROLE[] = {"Wedoogift", "Wedoofood"};
	String USER_PASSWORD_WITH_COMPANY_ROLE[] = {"wdg", "wdf"};

}
