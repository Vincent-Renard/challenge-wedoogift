package org.example.wedoodemo.endowment.service.food;

import org.example.wedoodemo.company.persistance.CompanyRepository;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.TestConstants;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;
import org.example.wedoodemo.user.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;


@Transactional
@SpringBootTest
@Sql("/sql/testLevel1.sql")
class MealVoucherServiceImplTest implements TestConstants {


	@Autowired
	MealVoucherService foodService;

	@Autowired
	UserService userService;

	@Autowired
	CompanyRepository companyRepository;


	@Transactional
	@Test
	@Rollback
	void distributeMealVoucher_GivenOkUser_GivenOkCompany_GivenOKValue_expectOK() {

		assertDoesNotThrow(() -> foodService.distributeMealVoucher(COMPANIES_IDS_OK[0],
				COMPANIES_IDS_OK[0], VALUE_GIVEN_TO_USER_1));
	}

	@Test
	@Rollback
	void distributeMealVoucher_GivenKOUser_GivenOkCompany_GivenOKValue_expectKO_throwsNoSuchUserException() {

		assertThrows(NoSuchUserException.class, () -> foodService.distributeMealVoucher(COMPANIES_IDS_OK[0],
				USER_ID_UNKNOWN, 1));

	}

	@Test
	@Rollback
	void distributeMealVoucher_GivenOKUser_GivenKoCompany_GivenOKValue_expectKO_throwsNoSuchCompanyException() throws NoSuchUserException {

		var userId = 1;
		var userBalance = userService.balanceOfUser(userId);
		assertThrows(NoSuchCompanyException.class, () -> foodService.distributeMealVoucher(COMPANY_ID_UNKNOWN, USER_IDS_OK[0], 1));
		assertEquals(userBalance, userService.balanceOfUser(userId), "In this case User blance must stay untouched");
	}

	@Transactional
	@Test
	@Rollback
	void distributeMealVoucher_GivenOkUser_GivenOkCompany_GivenKOValue_throwsUnsufficientCompanyBalanceException() throws NoSuchUserException {
		var userId = USER_IDS_OK[1];
		var userBalance = userService.balanceOfUser(userId);
		assertThrows(UnsufficientCompanyBalanceException.class, () -> foodService.distributeMealVoucher(COMPANIES_IDS_OK[1],
				userId, INITIAL_BALANCE_WEDOOFOOD + 1));
		assertEquals(userBalance, userService.balanceOfUser(userId), "In this case User blance must stay untouched");
	}
}