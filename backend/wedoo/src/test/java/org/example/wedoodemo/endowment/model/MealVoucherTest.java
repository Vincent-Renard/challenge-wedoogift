package org.example.wedoodemo.endowment.model;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@SpringBootTest
@Sql("/sql/testLevel1.sql")
class MealVoucherTest {

	private static final DateTimeFormatter DATEFORMATER = DateTimeFormatter.ofPattern("MM/dd/yyyy");

	public LocalDate expirationDateMealVoucher(LocalDate currentDate) {
		var nextYear = Year.of(currentDate.getYear() + 1);
		return LocalDate.of(nextYear.getValue(), Month.FEBRUARY,
				(nextYear.isLeap() ? 29 : 28));

	}

	@ParameterizedTest(name = "Test for meal voucher expiration date ")
	@CsvSource({"01/01/2020, 02/28/2021",
			"01/01/2019, 02/29/2020",
			"01/15/2020, 02/28/2021",
			"03/01/2020, 02/28/2021",
			"02/28/2021, 02/28/2022"})
	void expirationDateMealVoucher(String dateInput, String dateExpexted) {

		final var dateCurrent = LocalDate.parse(dateInput, DATEFORMATER);
		final var expectedDate = LocalDate.parse(dateExpexted, DATEFORMATER);

		assertEquals(expectedDate, expirationDateMealVoucher(dateCurrent));
	}
}