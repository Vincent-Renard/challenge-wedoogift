package org.example.wedoodemo.endowment.controler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.wedoodemo.SecurizedTestUtil;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.TestConstants;
import org.example.wedoodemo.endowment.controler.dto.in.EndowmentDto;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;
import org.example.wedoodemo.endowment.service.food.MealVoucherService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Rollback
class MealVoucherControlerTest extends SecurizedTestUtil implements TestConstants {


	private static final String URL = "/api/companies/{idCompany}/users/{idUser}/food";
	ObjectMapper om = new ObjectMapper();
	@MockBean
	private MealVoucherService mealServiceMock;
	@Autowired
	private MockMvc mockMvc;


	@Test
	void postMealVoucher_given_IdUserOK_IdCompanyOK_amountOK_Expect_204() throws Exception {
		var companyToken = getCompanyToken();

		var userId = USER_IDS_OK[1];
		var companyId = COMPANIES_IDS_OK[0];
		var amount = 80;

		doNothing().when(mealServiceMock).distributeMealVoucher(companyId, userId, amount);

		var inputDto = new EndowmentDto();
		inputDto.setAmount(amount);

		mockMvc.perform(MockMvcRequestBuilders.post(URL, companyId, userId)
						.header(HttpHeaders.AUTHORIZATION, companyToken)
						.contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inputDto)))
				.andExpect(MockMvcResultMatchers.status().isNoContent());

	}

	@Test
	void postMealVoucher_given_IdUserOK_IdCompanyOK_amountOK_WithUserToken_Expect_401() throws Exception {
		var userToken = getUserToken();
		var userId = USER_IDS_OK[1];
		var companyId = COMPANIES_IDS_OK[0];
		var amount = 80;

		doNothing().when(mealServiceMock).distributeMealVoucher(companyId, userId, amount);

		var inputDto = new EndowmentDto();
		inputDto.setAmount(amount);

		mockMvc.perform(MockMvcRequestBuilders.post(URL, companyId, userId)
						.header(HttpHeaders.AUTHORIZATION, userToken)
						.contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inputDto)))
				.andExpect(MockMvcResultMatchers.status().isForbidden());

	}

	@Test
	void postMealVoucher_given_IdUserOK_IdCompanyOK_amountOK_WithoutToken_Expect_403() throws Exception {

		var userId = USER_IDS_OK[1];
		var companyId = COMPANIES_IDS_OK[0];
		var amount = 80;

		doNothing().when(mealServiceMock).distributeMealVoucher(companyId, userId, amount);

		var inputDto = new EndowmentDto();
		inputDto.setAmount(amount);

		mockMvc.perform(MockMvcRequestBuilders.post(URL, companyId, userId)

						.contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inputDto)))
				.andExpect(MockMvcResultMatchers.status().isUnauthorized());

	}


	@Test
	void postMealVoucher_given_IdUserKO_IdCompanyOK_amountOK_Expect_404() throws Exception {
		var companyToken = getCompanyToken();

		var userId = USER_ID_UNKNOWN;
		var companyId = COMPANIES_IDS_OK[0];
		var amount = 80;

		doThrow(new NoSuchUserException(userId))
				.when(mealServiceMock)
				.distributeMealVoucher(companyId, userId, amount);

		var inputDto = new EndowmentDto();
		inputDto.setAmount(amount);
		mockMvc.perform(MockMvcRequestBuilders.post(URL, companyId, userId)
						.header(HttpHeaders.AUTHORIZATION, companyToken)
						.contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inputDto)))
				.andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	void postMealVoucher_given_IdUserOK_IdCompanyOK_amountOK_Expect_404() throws Exception {
		var companyToken = getCompanyToken();

		var userId = USER_IDS_OK[1];
		var companyId = COMPANY_ID_UNKNOWN;
		var amount = 80;

		doThrow(new NoSuchCompanyException(companyId))
				.when(mealServiceMock)
				.distributeMealVoucher(companyId, userId, amount);

		var inputDto = new EndowmentDto();
		inputDto.setAmount(amount);
		mockMvc.perform(MockMvcRequestBuilders.post(URL, companyId, userId)
						.header(HttpHeaders.AUTHORIZATION, companyToken)
						.contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inputDto)))
				.andExpect(MockMvcResultMatchers.status().isNotFound());
	}

	@Test
	void postMealVoucher_given_IdUserOK_IdCompanyOK_amountOK_Expect_409() throws Exception {
		var companyToken = getCompanyToken();

		var userId = USER_IDS_OK[1];
		var companyId = COMPANIES_IDS_OK[0];
		var amount = INITIAL_BALANCE_WEDOOGIFT + 1;

		doThrow(new UnsufficientCompanyBalanceException())
				.when(mealServiceMock)
				.distributeMealVoucher(companyId, userId, amount);

		var inputDto = new EndowmentDto();
		inputDto.setAmount(amount);
		mockMvc.perform(MockMvcRequestBuilders.post(URL, companyId, userId)
						.header(HttpHeaders.AUTHORIZATION, companyToken)
						.contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(inputDto)))
				.andExpect(MockMvcResultMatchers.status().isConflict());
	}

}