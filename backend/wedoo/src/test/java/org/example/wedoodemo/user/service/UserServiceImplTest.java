package org.example.wedoodemo.user.service;

import org.example.wedoodemo.company.persistance.CompanyRepository;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.TestConstants;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;
import org.example.wedoodemo.endowment.service.food.MealVoucherService;
import org.example.wedoodemo.endowment.service.giftcards.GiftCardService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
@Sql("/sql/testLevel1.sql")
class UserServiceImplTest implements TestConstants {
	@Autowired
	MealVoucherService foodService;
	@Autowired
	GiftCardService giftCardService;
	@Autowired
	UserService userService;

	@Autowired
	CompanyRepository companyRepository;


	@Test
	@Rollback
	void balanceOfUser_GivenKOUserId_expectNoSuchUserException() {
		assertThrows(NoSuchUserException.class, () -> userService.balanceOfUser(USER_ID_UNKNOWN));
	}


	@Test
	@Rollback
	void balanceOfUser_GivenOkUserId_WhenGiftCardDelivered_expectOk() throws NoSuchUserException, NoSuchCompanyException, UnsufficientCompanyBalanceException {
		var idUser = USER_IDS_OK[0];

		assertDoesNotThrow(() -> userService.balanceOfUser(idUser));
		giftCardService.distributeGiftCard(COMPANIES_IDS_OK[0], idUser, VALUE_GIVEN_TO_USER_1);
		assertEquals(userService.balanceOfUser(idUser), INITIAL_AMOUNT_GIVEN_TO_USER_1 + VALUE_GIVEN_TO_USER_1);

	}


	@Test
	@Rollback
	void balanceOfUser_GivenOkUserId_WhenMealVoucherDelivered_expectOk() throws NoSuchUserException, NoSuchCompanyException, UnsufficientCompanyBalanceException {
		var idUser = USER_IDS_OK[0];
		var amountMealVoucherGifted = 250;

		assertDoesNotThrow(() -> userService.balanceOfUser(idUser));
		foodService.distributeMealVoucher(COMPANIES_IDS_OK[0], idUser, amountMealVoucherGifted);
		assertEquals(userService.balanceOfUser(idUser), INITIAL_AMOUNT_GIVEN_TO_USER_1 + amountMealVoucherGifted);

	}

	@Test
	@Rollback
	void balanceOfUser_GivenOkParams_WhenGiftCardDelivered_expectCompanyBalanceChanged() throws NoSuchUserException, NoSuchCompanyException, UnsufficientCompanyBalanceException {

		var idUser = USER_IDS_OK[0];
		var companyId = COMPANIES_IDS_OK[0];

		assertDoesNotThrow(() -> userService.balanceOfUser(idUser));
		giftCardService.distributeGiftCard(companyId, idUser, VALUE_GIVEN_TO_USER_1);
		assertEquals(userService.balanceOfUser(idUser), INITIAL_AMOUNT_GIVEN_TO_USER_1 + VALUE_GIVEN_TO_USER_1);
		var companyBalance = companyRepository.getById(companyId).getBalance();
		assertEquals(INITIAL_BALANCE_WEDOOGIFT - VALUE_GIVEN_TO_USER_1, companyBalance);
	}

	@Test
	@Rollback
	void balanceOfUser_GivenOkParams_WhenMealVoucherDelivered_expectCompanyBalanceChanged() throws NoSuchUserException, NoSuchCompanyException, UnsufficientCompanyBalanceException {

		var idUser = USER_IDS_OK[0];
		var companyId = COMPANIES_IDS_OK[0];

		var amountMealVoucher = 250;
		assertDoesNotThrow(() -> userService.balanceOfUser(idUser));
		foodService.distributeMealVoucher(companyId, idUser, amountMealVoucher);
		assertEquals(userService.balanceOfUser(idUser), INITIAL_AMOUNT_GIVEN_TO_USER_1 + amountMealVoucher);

		var companyBalance = companyRepository.getById(companyId).getBalance();

		assertEquals(INITIAL_BALANCE_WEDOOGIFT - amountMealVoucher, companyBalance);
	}

}