package org.example.wedoodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WedoodemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(WedoodemoApplication.class, args);
	}
}
