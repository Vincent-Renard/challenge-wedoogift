package org.example.wedoodemo.user.service;

import lombok.RequiredArgsConstructor;
import org.example.wedoodemo.company.persistance.CompanyRepository;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.user.persistance.UserRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
	private final UserRepository users;

	@Override
	public int balanceOfUser(int idUser) throws NoSuchUserException {
		return users.findById(idUser)
				.orElseThrow(() -> new NoSuchUserException(idUser)).getBalance();
	}
}
