package org.example.wedoodemo.endowment.model;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.example.wedoodemo.company.persistance.model.Company;

import javax.persistence.Entity;
import java.time.LocalDate;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@Entity
@NoArgsConstructor
public class GiftCard extends Endowment {

	private static final int LIFESPAN = 365;

	public GiftCard(int amount, Company company) {
		super(amount, company, LocalDate.now().plusDays(LIFESPAN));
	}

	@Override
	public String toString() {
		return "GiftCard{" +
				"id=" + id +
				", endDate=" + endDate +
				", startDate=" + startDate +
				", amount=" + amount +
				", user=" + user +
				", company=" + company +
				'}';
	}
}
