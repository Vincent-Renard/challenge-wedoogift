package org.example.wedoodemo.endowment.persistance;

import org.example.wedoodemo.endowment.model.Endowment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EndowmentRepository extends JpaRepository<Endowment, Integer> {
}
