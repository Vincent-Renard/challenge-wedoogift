package org.example.wedoodemo.user.controler;


import lombok.AllArgsConstructor;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.user.controler.dto.UserBalanceDto;
import org.example.wedoodemo.user.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class UserController {

	private final UserService userService;

	@GetMapping("/{idUser}/balance")
	public ResponseEntity<UserBalanceDto> getUserBalance(@PathVariable int idUser) throws NoSuchUserException {

		return ResponseEntity.ok(UserBalanceDto.from(userService.balanceOfUser(idUser)));

	}

}
