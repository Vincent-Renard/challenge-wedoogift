package org.example.wedoodemo.core.security.config.filter;

import org.example.wedoodemo.core.security.config.token.decoder.JwtsDecoder;
import org.example.wedoodemo.core.security.config.token.encoder.JwtsEncoder;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtsAuthenticationFilter extends UsernamePasswordAuthenticationFilter {


	private static final String AUTH_URL = "/api/login";
	private JwtsEncoder jwtsEncoder;

	public JwtsAuthenticationFilter(AuthenticationManager authenticationManager, JwtsEncoder jwtsEncoder) {
		setAuthenticationManager(authenticationManager);
		setFilterProcessesUrl(AUTH_URL);
		this.jwtsEncoder = jwtsEncoder;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
		var userDetails = (UserDetails) authResult.getPrincipal();
		var token = jwtsEncoder.genTokenFromUserDetails(userDetails);
		response.addHeader(HttpHeaders.AUTHORIZATION, token);
	}
}