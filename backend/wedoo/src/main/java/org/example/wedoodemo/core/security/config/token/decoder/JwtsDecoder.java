package org.example.wedoodemo.core.security.config.token.decoder;

import org.example.wedoodemo.core.security.exceptions.InvalidTokenException;
import org.example.wedoodemo.core.security.exceptions.NoTokenException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public interface JwtsDecoder {


	 UsernamePasswordAuthenticationToken decodeToken(String token) throws InvalidTokenException, NoTokenException ;
	}
