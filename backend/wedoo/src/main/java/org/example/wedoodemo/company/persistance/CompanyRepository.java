package org.example.wedoodemo.company.persistance;

import org.example.wedoodemo.company.persistance.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Integer> {

}
