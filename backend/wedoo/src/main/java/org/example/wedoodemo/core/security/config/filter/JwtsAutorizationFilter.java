package org.example.wedoodemo.core.security.config.filter;

import org.example.wedoodemo.core.security.config.token.decoder.JwtsDecoder;
import org.example.wedoodemo.core.security.exceptions.InvalidTokenException;
import org.example.wedoodemo.core.security.exceptions.NoTokenException;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtsAutorizationFilter extends BasicAuthenticationFilter {


	private JwtsDecoder jwtsDecoder;

	public JwtsAutorizationFilter(AuthenticationManager authenticationManager, JwtsDecoder jwtsDecoder) {
		super(authenticationManager);
		this.jwtsDecoder = jwtsDecoder;
	}


	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
		var token = request.getHeader(HttpHeaders.AUTHORIZATION);
		try {
			var authentication = jwtsDecoder.decodeToken(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
		} catch (InvalidTokenException | NoTokenException invalidToken) {
			SecurityContextHolder.clearContext();
		}


		chain.doFilter(request, response);
	}
}
