package org.example.wedoodemo.core.security.config.token.decoder;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.example.wedoodemo.core.security.exceptions.InvalidTokenException;
import org.example.wedoodemo.core.security.exceptions.NoTokenException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class JwtsDecoderImpl implements JwtsDecoder {


	private final Key secret;
	@Value("${auth.token.prefix}")
	private String tokenPrefix;

	@Override
	public UsernamePasswordAuthenticationToken decodeToken(String token) throws InvalidTokenException, NoTokenException {
		if (token == null || token.isBlank()) {
			SecurityContextHolder.clearContext();
			throw new NoTokenException();
		}
		if (token.startsWith(tokenPrefix)) {
			token = token.replace(tokenPrefix, "").trim();
		}
		try {

			Jws<Claims> clms = Jwts.parserBuilder()
					.setSigningKey(secret).build()
					.parseClaimsJws(token);

			var body = clms.getBody();
			var login = body.getSubject();
			List<String> roles = body.get("roles", List.class);
			List<SimpleGrantedAuthority> authorities = roles.stream()
					.map(SimpleGrantedAuthority::new)
					.collect(Collectors.toList());

			return new UsernamePasswordAuthenticationToken(login, null, authorities);
		} catch (JwtException e) {
			throw new InvalidTokenException();
		}	}
}
