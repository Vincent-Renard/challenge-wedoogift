package org.example.wedoodemo.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Unknow User id")
public class NoSuchUserException extends Exception {
	public NoSuchUserException(long id) {
		super(String.format("User with id %d not found", id));
	}
}
