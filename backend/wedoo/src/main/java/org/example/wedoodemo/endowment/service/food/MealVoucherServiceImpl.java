package org.example.wedoodemo.endowment.service.food;

import lombok.RequiredArgsConstructor;
import org.example.wedoodemo.company.persistance.CompanyRepository;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;
import org.example.wedoodemo.endowment.model.MealVoucher;
import org.example.wedoodemo.user.persistance.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class MealVoucherServiceImpl implements MealVoucherService {

	private final CompanyRepository compagnies;
	private final UserRepository users;


	@Transactional
	@Override
	public void distributeMealVoucher(int idCompany, int idUser, int value) throws NoSuchCompanyException, NoSuchUserException, UnsufficientCompanyBalanceException {
		var company = compagnies.findById(idCompany)
				.orElseThrow(() -> new NoSuchCompanyException(idCompany));

		if (company.getBalance() < value) {
			throw new UnsufficientCompanyBalanceException();
		}

		var user = users.findById(idUser)
				.orElseThrow(() -> new NoSuchUserException(idUser));
		user.recieveEndowement(new MealVoucher(value, company));
		company.debit(value);
		users.save(user);
	}
}
