package org.example.wedoodemo.endowment.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.example.wedoodemo.company.persistance.model.Company;
import org.example.wedoodemo.user.persistance.model.User;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@FieldDefaults(level = AccessLevel.PROTECTED)
@Getter
@Setter
@Entity
@Inheritance
@NoArgsConstructor
public abstract class Endowment {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	LocalDate endDate;

	//@CreationTimestamp
	LocalDate startDate;

	int amount;

	@ManyToOne
	User user;


	@ManyToOne
	Company company;

	protected Endowment(int amount, Company company, LocalDate expirationDate) {
		this.startDate = LocalDate.now();
		this.endDate = expirationDate;
		this.amount = amount;
		this.company = company;
	}

	public boolean isExpired() {
		return this.endDate.isBefore(LocalDate.now());
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		Endowment endowment = (Endowment) obj;
		return amount == endowment.amount && endDate.equals(endowment.endDate) && user.equals(endowment.user);
	}

	@Override
	public int hashCode() {
		return Objects.hash(endDate, amount);
	}

	@Override
	public String toString() {
		return "Endowment{" +
				"id=" + id +
				", endDate=" + endDate +
				", startDate=" + startDate +
				", amount=" + amount +
				", user=" + ((user == null) ? " null" : user.getId()) +
				", company=" + ((company == null) ? " null" : company.getId()) +
				'}';
	}
}
