package org.example.wedoodemo.core.security.config.token.encoder;

import org.springframework.security.core.userdetails.UserDetails;

public interface JwtsEncoder {


	 String genTokenFromUserDetails(UserDetails userDetails) ;
}
