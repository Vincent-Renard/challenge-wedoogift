package org.example.wedoodemo.core.security.config;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.example.wedoodemo.core.security.config.filter.JwtsAuthenticationFilter;
import org.example.wedoodemo.core.security.config.filter.JwtsAutorizationFilter;
import org.example.wedoodemo.core.security.config.token.decoder.JwtsDecoder;
import org.example.wedoodemo.core.security.config.token.encoder.JwtsEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.crypto.SecretKey;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtsDecoder jwtsDecoder;

	@Autowired
	private JwtsEncoder jwtsEncoder;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.inMemoryAuthentication().withUser("Richard")
				.password(passwordEncoder().encode("Riri"))
				.roles("USER")
				.and().withUser("Firmin")
				.password(passwordEncoder().encode("Fifi"))
				.roles("USER")
				.and().withUser("Louis")
				.password(passwordEncoder().encode("Loulou"))
				.roles("USER")
				.and().withUser("Wedoogift")
				.password(passwordEncoder().encode("wdg"))
				.roles("COMPANY")
				.and().withUser("Wedoofood")
				.password(passwordEncoder().encode("wdf"))
				.roles("COMPANY");

	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http
				.csrf().disable()
				.addFilter(new JwtsAutorizationFilter(authenticationManager(),jwtsDecoder))
				.addFilter(new JwtsAuthenticationFilter(authenticationManager(), jwtsEncoder))
				.authorizeRequests()
				.antMatchers(HttpMethod.GET, "/api/users/**").hasRole("USER")
				.antMatchers(HttpMethod.POST, "/api/companies/**").hasRole("COMPANY")
				.antMatchers(HttpMethod.POST, "/api/login/**").permitAll()
				.anyRequest().denyAll()
				.and()
				.httpBasic()
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}


	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}


	@Bean
	public SecretKey getSecretKey() {
		return Keys.secretKeyFor(SignatureAlgorithm.HS512);
	}
}
