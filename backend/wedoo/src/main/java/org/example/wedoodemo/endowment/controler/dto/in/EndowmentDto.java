package org.example.wedoodemo.endowment.controler.dto.in;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EndowmentDto {
	int amount;
}
