package org.example.wedoodemo.user.controler.dto;

import lombok.Getter;

import java.io.Serializable;


@Getter
public final class UserBalanceDto implements Serializable {
	private int balance;

	private UserBalanceDto(int balance) {
		this.balance = balance;
	}

	public static UserBalanceDto from(int balanceOfUser) {
		return new UserBalanceDto(balanceOfUser);
	}
}
