package org.example.wedoodemo.user.service;

import org.example.wedoodemo.core.exceptions.NoSuchUserException;

public interface UserService {
	int balanceOfUser(int idUser) throws NoSuchUserException;
}
