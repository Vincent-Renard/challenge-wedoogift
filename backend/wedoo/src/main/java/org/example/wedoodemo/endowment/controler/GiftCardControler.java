package org.example.wedoodemo.endowment.controler;

import lombok.AllArgsConstructor;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.controler.dto.in.EndowmentDto;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;
import org.example.wedoodemo.endowment.service.giftcards.GiftCardService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/companies/{idCompany}/users/{idUser}", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class GiftCardControler {

	private final GiftCardService giftCardService;

	@PostMapping(value = "/gifts", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> postGiftCard(@PathVariable int idCompany, @PathVariable int idUser, @RequestBody EndowmentDto gift) throws NoSuchUserException, NoSuchCompanyException, UnsufficientCompanyBalanceException {
		giftCardService.distributeGiftCard(idCompany, idUser, gift.getAmount());
		return ResponseEntity.noContent().build();
	}


}
