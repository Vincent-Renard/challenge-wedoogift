package org.example.wedoodemo.endowment.model;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.example.wedoodemo.company.persistance.model.Company;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@Entity
@NoArgsConstructor
public class MealVoucher extends Endowment {

	private static LocalDate expirationDateMealVoucher() {
		var currentDate = LocalDate.now();
		var nextYear = Year.of(currentDate.getYear() + 1);
		return LocalDate.of(nextYear.getValue(), Month.FEBRUARY,
				(nextYear.isLeap() ? 29 : 28));
	}

	public MealVoucher(int amount, Company company) {
		super(amount, company, expirationDateMealVoucher());
	}


}
