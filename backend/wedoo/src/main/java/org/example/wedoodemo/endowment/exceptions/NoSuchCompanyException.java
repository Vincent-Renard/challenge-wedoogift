package org.example.wedoodemo.endowment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Unknow Company id")
public class NoSuchCompanyException extends Exception {
	public NoSuchCompanyException(long id) {
		super(String.format("Company with id %d not found", id));
	}
}
