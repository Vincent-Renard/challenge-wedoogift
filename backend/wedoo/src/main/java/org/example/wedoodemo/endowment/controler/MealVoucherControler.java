package org.example.wedoodemo.endowment.controler;

import lombok.AllArgsConstructor;
import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.controler.dto.in.EndowmentDto;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;
import org.example.wedoodemo.endowment.service.food.MealVoucherService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api/companies/{idCompany}/users/{idUser}", produces = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class MealVoucherControler {

	private final MealVoucherService mealVoucherService;

	@PostMapping(value = "/food", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> postMealVoucher(@PathVariable int idCompany, @PathVariable int idUser, @RequestBody EndowmentDto food) throws NoSuchUserException,
			NoSuchCompanyException,
			UnsufficientCompanyBalanceException {
		mealVoucherService.distributeMealVoucher(idCompany, idUser, food.getAmount());
		return ResponseEntity.noContent().build();
	}


}
