package org.example.wedoodemo.core.security.config.token.encoder;

import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Component
public class JwtsEncoderImpl implements JwtsEncoder {


	private final Key secret;
	@Value("${auth.token.prefix}")
	private String tokenPrefix;
	@Value("${auth.token.ttl}")
	private String tokenTTL;
	@Override
	public String genTokenFromUserDetails(UserDetails userDetails) {

		var claims = Jwts.claims().setSubject(userDetails.getUsername());

		claims.put("roles", userDetails.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toList()));

		var jwts = Jwts.builder()
				.setClaims(claims)
				.setExpiration(new Date(System.currentTimeMillis() + Integer.parseInt(tokenTTL)))
				.signWith(secret)
				.compact();

		return String.format("%s %s", tokenPrefix, jwts);	}
}
