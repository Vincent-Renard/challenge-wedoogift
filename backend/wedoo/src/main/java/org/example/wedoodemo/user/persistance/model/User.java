package org.example.wedoodemo.user.persistance.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.example.wedoodemo.endowment.model.Endowment;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@ToString
public class User implements Serializable {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	Integer id;

	@Getter
	int initialDeposit;

	@Transient
	int balance;

	@Getter
	@OneToMany(mappedBy = "user", cascade = {CascadeType.ALL})
	@ToString.Exclude
	List<Endowment> endowments = new ArrayList<>();


	public void recieveEndowement(Endowment distribution) {
		if (endowments == null) {
			endowments = new ArrayList<>();
		}
		distribution.setUser(this);
		endowments.add(distribution);
	}

	public int getBalance() {
		this.balance = this.computeBalance();
		return balance;
	}

	public int computeBalance() {
		return initialDeposit + (endowments != null ? endowments.stream()
				.filter(endowment -> !endowment.isExpired())
				.map(Endowment::getAmount)
				.mapToInt(Integer::intValue)
				.sum() : 0);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || getClass() != obj.getClass()) return false;
		User user = (User) obj;
		return id.equals(user.id) && Objects.equals(endowments, user.endowments);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, endowments);
	}
}
