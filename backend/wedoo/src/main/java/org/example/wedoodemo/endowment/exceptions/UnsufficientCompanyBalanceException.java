package org.example.wedoodemo.endowment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Gift Amount Error ")
public class UnsufficientCompanyBalanceException extends Exception {
	public UnsufficientCompanyBalanceException() {
		super("Company Balance Unsufficient");
	}
}
