package org.example.wedoodemo.user.persistance;

import org.example.wedoodemo.user.persistance.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
