package org.example.wedoodemo.endowment.service.giftcards;

import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;

public interface GiftCardService {

	void distributeGiftCard(int idCompany, int idUser, int value) throws NoSuchCompanyException,
			NoSuchUserException,
			UnsufficientCompanyBalanceException;
}
