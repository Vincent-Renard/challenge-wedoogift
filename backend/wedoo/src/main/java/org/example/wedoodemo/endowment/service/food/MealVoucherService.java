package org.example.wedoodemo.endowment.service.food;

import org.example.wedoodemo.core.exceptions.NoSuchUserException;
import org.example.wedoodemo.endowment.exceptions.NoSuchCompanyException;
import org.example.wedoodemo.endowment.exceptions.UnsufficientCompanyBalanceException;

public interface MealVoucherService {
	/**
	 * @param idCompany
	 * @param idUser
	 * @param value     Value of the distribution
	 * @throws NoSuchCompanyException              If this company does not exist
	 * @throws NoSuchUserException                 If this user does not exist
	 * @throws UnsufficientCompanyBalanceException If value can not be delivered
	 */
	void distributeMealVoucher(int idCompany, int idUser, int value) throws NoSuchCompanyException,
			NoSuchUserException,
			UnsufficientCompanyBalanceException;
}
